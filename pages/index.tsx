import React from "react";
import { NextPage } from "next";
import HomepageFooter from "module/Common/layout/Footer";
import { BasicInput } from "components/Input";
import { BasicButton } from "components/Button";
import { useLazyQuery } from "@apollo/client";
import { LOGIN } from "graphql/Queries/User";
import { LoginPayload, LoginVariables } from "types/Queries/User";
import { setToken } from "services/AuthenticationHelper";
import { useRouter } from "next/router";

const RootPage: NextPage = () => {
  const [email, setEmail] = React.useState("");
  const [password, setPassword] = React.useState("");
  const router = useRouter();
  const [login, { loading }] = useLazyQuery<
    LoginPayload,
    LoginVariables
  >(LOGIN, {
    onCompleted: (res) => {
      setToken(res.login.token);
      return router.push("/admin");
    },
  });

  return (
    <section className="w-screen min-h-full-v flex flex-col">
      <div className="flex-1 flex flex-col items-center justify-center">
        <div className="w-full flex flex-col items-center max-w-3xl p-8 rounded shadow-xl">
          <div className="flex pb-5 justify-center font-bold text-xl mb-2 text-center lg:text-left">
            Identifiez-vous
          </div>
          <div className="flex pb-5 w-4/5">
            <BasicInput
              placeholder="Email"
              onChange={(value) => setEmail(value)}
            />
          </div>
          <div className="flex pb-5 w-4/5">
            <BasicInput
              placeholder="Mot de passe"
              onChange={(value) => setPassword(value)}
            />
          </div>
          <BasicButton
            isDisabled={email === "" || password === "" || loading}
            onClick={() => login({ variables: { email, password } })}
          >
            Se connecter
          </BasicButton>
        </div>
      </div>
      <HomepageFooter />
    </section>
  );
};

export default RootPage;
