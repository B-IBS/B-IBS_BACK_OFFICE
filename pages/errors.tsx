import React, { useEffect } from "react";
import { NextPage } from "next";
import HomepageFooter from "module/Common/layout/Footer";
import { getToken } from "services/AuthenticationHelper";
import { useRouter } from "next/router";
import ErrorTable from "module/Errors/ErrorTable";
import Sidebar from "module/Common/Sidebar";

const RootPage: NextPage = () => {
  const router = useRouter();
  const token = getToken();

  useEffect(() => {
    if (!token) {
      router.push("/");
    }
  }, [router]);

  if (!token) return null;

  return (
    <section className="w-screen min-h-full-v flex flex-col">
      <div className="flex">
        <Sidebar />
        <div className="w-5/6 p-5">
          <ErrorTable />
        </div>
      </div>
      <HomepageFooter />
    </section>
  );
};

export default RootPage;
