import "../styles/index.css";

import { AppProps } from "next/app";
import { ApolloProvider } from "@apollo/client";
import { useApollo } from "services/ApolloClient";

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const MyApp = ({ Component, pageProps }: AppProps) => {
  const apolloClient = useApollo(pageProps.initialApolloState);

  return (
    <ApolloProvider client={apolloClient}>
      <Component {...pageProps} />
    </ApolloProvider>
  );
};

export default MyApp;
