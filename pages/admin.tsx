import React, { useEffect } from "react";
import { NextPage } from "next";
import HomepageFooter from "module/Common/layout/Footer";
import { getToken } from "services/AuthenticationHelper";
import { useRouter } from "next/router";
import Sidebar from "module/Common/Sidebar";

const RootPage: NextPage = () => {
  const router = useRouter();

  useEffect(() => {
    if (!getToken()) {
      router.push("/")
    }
  }, [router]);
  return (
    <section className="w-screen min-h-full-v flex flex-col">
      <div className="flex">
        <Sidebar />
        <div className="flex-auto"></div>
      </div>
      <HomepageFooter />
    </section>
  );
};

export default RootPage;
