import { gql } from "@apollo/client";

export const GET_REQUEST_METRICS = gql`
    query getMetricsRequestStat {
        getMetricsRequestStat {
            stats { 
                name
                count
            }
        }
    }
`;
