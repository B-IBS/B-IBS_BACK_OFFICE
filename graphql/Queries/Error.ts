import { gql } from "@apollo/client";

export const GET_ALL_ERRORS = gql`
  query getAllErrors {
    getAllErrors {
      id
      createdAt
      updatedAt
      code
      name
      requestName
      args
      error
    }
  }
`;
