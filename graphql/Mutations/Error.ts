import { gql } from "@apollo/client";

export const DELETE_ERROR = gql`
  mutation deleteError($id: ID!) {
    deleteError(id: $id)
  }
`;
