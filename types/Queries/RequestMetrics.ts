interface MetricRequestElement {
  name: string;
  count: number;
}

interface MetricRequestLog {
  id: string;
  createdAt: string;
  updatedAt: string;
  name: string;
}

export interface MetricRequestStat {
  stats: MetricRequestElement[];
}

export interface GetAllMetricsPayload {
  getMetricsRequestStat: MetricRequestStat;
}
