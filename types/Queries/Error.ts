export interface ErrorLog {
  id: string;
  createdAt: string;
  updatedAt: string;
  code: number;
  name: string;
  requestName: string;
  args?: { [key: string]: string | number };
  error: string;
}

export interface GetAllErrorsPayload {
  getAllErrors: ErrorLog[];
}
