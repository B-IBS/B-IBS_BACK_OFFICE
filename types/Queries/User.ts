export interface LoginVariables {
  email: string;
  password: string;
}

export interface LoginPayload {
  login: {
    token: string;
  };
}

