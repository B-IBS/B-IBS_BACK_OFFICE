import React from "react";
import ReactDOM from "react-dom";
import Chart from "../chart";

import {cleanup, render} from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

afterEach(cleanup);

it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<Chart></Chart>, div)
})
