import React from "react";
import ReactDOM from "react-dom";
import SignInside from "../tokenLogin"

import {cleanup, render} from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import SignInSide from "../tokenLogin";

afterEach(cleanup);

it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<SignInSide/>, div)
})

it("have token textfield", () => {
    const {getByTestId} = render(<SignInSide/>);
    expect(getByTestId("title")).toHaveTextContent("caca");
})