import { useMutation, useQuery } from "@apollo/client";
import { GetAllErrorsPayload } from "types/Queries/Error";
import BasicTable from "components/Table/BasicTable";
import React, { useEffect, useMemo, useState } from "react";
import { BasicButton } from "components/Button";
import { DELETE_ERROR } from "graphql/Mutations/Error";
import { ResponsiveBar } from "@nivo/bar";
import { GET_REQUEST_METRICS } from "../../graphql/Queries/RequestMetrics";
import {
  GetAllMetricsPayload,
  MetricRequestStat,
} from "../../types/Queries/RequestMetrics";

const MyResponsiveBar = ({ data }: { data: MetricRequestStat }) => (
  <ResponsiveBar
    data={data.stats as any}
    indexBy="name"
    keys={["count"]}
    margin={{ top: 50, right: 130, bottom: 50, left: 60 }}
    padding={0.2}
    layout="horizontal"
    valueScale={{ type: "linear" }}
    indexScale={{ type: "band", round: true }}
    // valueFormat={{ format: "", enabled: false }}
    colors={{ scheme: "red_yellow_blue" }}
    borderColor={{ from: "color", modifiers: [["darker", 1.6]] }}
    axisTop={null}
    axisRight={null}
    axisBottom={{
      tickSize: 2,
      tickPadding: 10,
      tickRotation: 0,
      legend: "Number of usage",
      legendPosition: "middle",
      legendOffset: 32,
    }}
    axisLeft={{
      tickSize: 5,
      tickPadding: 5,
      tickRotation: 0,
      legend: "",
      legendPosition: "middle",
      legendOffset: -40,
    }}
    labelSkipWidth={12}
    labelSkipHeight={12}
    labelTextColor={{ from: "color", modifiers: [["darker", 1.6]] }}
    legends={[
      {
        dataFrom: "keys",
        anchor: "bottom-right",
        direction: "column",
        justify: false,
        translateX: 120,
        translateY: 0,
        itemsSpacing: 2,
        itemWidth: 100,
        itemHeight: 20,
        itemDirection: "left-to-right",
        itemOpacity: 0.85,
        symbolSize: 20,
        effects: [
          {
            on: "hover",
            style: {
              itemOpacity: 1,
            },
          },
        ],
      },
    ]}
  />
);

export const RequestMetricsTable = () => {
  const { data } = useQuery<GetAllMetricsPayload>(GET_REQUEST_METRICS, {
    fetchPolicy: "network-only",
  });
  console.log(data?.getMetricsRequestStat);

  if (!data) return <span>Loading</span>;
  console.log(data.getMetricsRequestStat.stats);
  return <MyResponsiveBar data={data.getMetricsRequestStat} />;
};

export default RequestMetricsTable;
