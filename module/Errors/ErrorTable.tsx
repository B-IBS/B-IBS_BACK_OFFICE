import { useMutation, useLazyQuery } from "@apollo/client";
import { GET_ALL_ERRORS } from "graphql/Queries/Error";
import { ErrorLog, GetAllErrorsPayload } from "types/Queries/Error";
import BasicTable from "components/Table/BasicTable";
import React, { useEffect, useMemo, useState } from "react";
import { BasicButton } from "components/Button";
import { DELETE_ERROR } from "graphql/Mutations/Error";

export const ErrorTable = () => {
  const [errors, setErrors] = useState<
    Array<Omit<ErrorLog, "args"> & { action: string; args: string }>
  >([]);
  const [getErrors, { data }] = useLazyQuery<GetAllErrorsPayload>(
    GET_ALL_ERRORS,
    {
      fetchPolicy: "network-only",
      onCompleted: (res) =>
        res.getAllErrors &&
        setErrors(
          res.getAllErrors.map((e) => ({
            ...e,
            action: "Supprimer",
            args: JSON.stringify(e.args),
          }))
        ),
    }
  );
  const [deleteError] = useMutation(DELETE_ERROR, {
    onCompleted: (isDeleted) => isDeleted && getErrors(),
  });
  useEffect(() => {
    getErrors();
  }, []);

  const errorTableColumns = useMemo(
    () => [
      {
        Header: "ID",
        accessor: "id",
      },
      {
        Header: "Name",
        accessor: "name",
      },
      {
        Header: "Request name",
        accessor: "request name",
      },
      {
        Header: "Error",
        accessor: "error",
      },
      {
        Header: "Arguments",
        accessor: "args",
      },
      {
        Header: "Created at",
        accessor: "createdAt",
      },
      {
        Header: "Supprimer",
        accessor: "action",
        Cell: ({ cell }: { cell: any }) => (
          <BasicButton
            onClick={async () => {
              await deleteError({ variables: { id: cell.row.values.id } });
            }}
          >
            {cell.row.values.action}
          </BasicButton>
        ),
      },
    ],
    []
  );

  if (!data) return <span>Loading</span>;

  return <BasicTable columns={errorTableColumns} data={errors} />;
};

export default ErrorTable;
