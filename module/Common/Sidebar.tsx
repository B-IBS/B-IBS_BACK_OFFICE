import Link from "next/link";

const links = [
  {
    name: "Errors",
    href: "/errors",
  },
  {
    name: "Request metrics",
    href: "/request-metrics",
  },
];

const Sidebar = () => {
  return (
    <div className="w-1/6 bg-gray-100 min-h-screen font-mono">
      <div className="flex flex-col items-center">
        <span className="text-4xl">Links</span>
        {links.map((link) => (
          <Link key={`link_sidebar_${link.name}`} href={link.href}>
            {link.name}
          </Link>
        ))}
      </div>
    </div>
  );
};

export default Sidebar;
