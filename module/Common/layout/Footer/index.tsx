import React from "react";

const HomepageFooter = () => (
  <div className="min-h-1/20 flex justify-center items-center text-gray-500 italic border border-grey-200 text-center text-sm mt-4">
    2020 B.IBS inc, tout droits réservés, en particulier Epitech Montpelier
  </div>
);

export default HomepageFooter;
