import React from "react";

interface IBasicButton {
  isDisabled?: boolean;
  children: React.ReactNode;
  onClick: () => void;
  tw?: string;
  style?: React.CSSProperties;
}

const setColor = (isDisabled: boolean) =>
  isDisabled ? "gray-500" : "green-BIBS";

const BasicButton = ({ isDisabled, children, onClick, tw, style }: IBasicButton) => {
  return (
    <button
      style={style}
      type="button"
      className={`text-${setColor(
        isDisabled || false
      )} flex justify-center items-center ${tw}`}
      onClick={onClick}
      disabled={isDisabled}
    >
      {children}
    </button>
  );
};

export default BasicButton;
