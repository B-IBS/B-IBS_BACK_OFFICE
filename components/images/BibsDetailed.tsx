const BibsDetails: React.FC = () => (
  <img
    src={require("../../public/LOGO_BIBS_01.svg")}
    alt="B.IBS logo with acronym"
  />
);

export default BibsDetails;
