const BibsLogo: React.FC = () => (
  <img src={require("../../public/LOGO_BIBS_04.svg")} alt="B.IBS logo" />
);

export default BibsLogo;
