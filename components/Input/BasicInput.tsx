import React from "react";

interface IBasicInput {
  placeholder: string;
  onChange: (value: string) => void;
}

const BasicInput = ({ placeholder, onChange }: IBasicInput) => {
  return (
    <input
      type="text"
      placeholder={placeholder}
      className="transition duration-300 ease-in-out appearance-none w-full bg-gray-200 text-gray-700 border-2 border-gray-200 rounded py-3 px-4 focus:outline-none focus:bg-white focus:border-green-500"
      onChange={(e) => onChange(e.target.value)}
    />
  );
};

export default BasicInput;
