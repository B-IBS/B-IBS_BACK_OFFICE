module.exports = {
    purge: ['./components/**/*.tsx', './pages/**/*.tsx', './module/**/*.tsx'],
    theme: {
        extend: {
            minHeight: {
                '0': '0',
                '1/4': '25%',
                '1/2': '50%',
                '3/4': '75%',
                'full': '100%',
                'full-v': '100vh',
                '1/10': '10%',
                '9/10': '90%',
                '9/10-v': '90vh',
                '1/3': '33%',
                '2/3': '67%',
                '1/10': '10%',
                '1/20': '5%'
            },
            minWidth: {
                '0': '0',
                '1/4': '25%',
                '1/2': '50%',
                '3/4': '75%',
                'full': '100%',
            },
            maxWidth: {
                '1/4': '25%',
                '1/2': '50%',
                '3/4': '75%',
                '1/5': '20%',
                '4/5': '80%',
            },
            colors: {
                green: {
                    'BIBS': '#88BF8B'
                },
                'bibs-primary': '#88BE8C',
                'bibs-secondary': '#B8E889',
                'bibs-warning': '#FFC97A',
                'bibs-crisis-light': '#FF6E79',
                'bibs-crisis-medium': '#AA3838',
                'bibs-crisis-strong': '#5E2A2A',
                'bibs-text': '#272727',
                'bibs-hint': '#BABABA',
            },
            height: {
                'graph': '50vh'
            },
            width: {
                'graph': '80vw'
            },
            borderRadius: {
              '3xl': '1.5rem'
            }
        }
    },
    variants: {},
    plugins: [],
}
